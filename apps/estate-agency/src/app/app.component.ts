import { Component } from '@angular/core';

@Component({
  selector: 'estate-agency-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'estate-agency';
}
